#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <nlohmann/json.hpp>

// for convenience
using json = nlohmann::json;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    json j = {
      {"pi", 3.141},
      {"happy", true},
      {"name", "Niels"},
      {"nothing", nullptr},
      {"answer", {
        {"everything", 42}
      }},
      {"list", {1, 0, 2}},
      {"object", {
        {"currency", "USD"},
        {"value", 42.99}
      }}
    };

    std::string s = j.dump();
    ui->label->setText(s.c_str());
}

MainWindow::~MainWindow()
{
    delete ui;
}
